APP_NAME=java-demo-app
CURRENT_WORKING_DIR=$(shell pwd)

QUAY_REPO=digitalonus
QUAY_USERNAME?="unknown"
QUAY_PASSWORD?="unknown"

GO_PIPELINE_COUNTER?="unknown"

# Construct the image tag.
VERSION=0.1

# Construct docker image name.
IMAGE = $(QUAY_REPO)/$(APP_NAME)

build: build-app clean-app build-image

push: docker-login push-image docker-logout

deploy-dev: helm-package deploy

build-app:
	docker build -t build-img:$(VERSION) -f Dockerfile.build .
	docker run --name build-image-$(VERSION) --rm -v $(CURRENT_WORKING_DIR)/src:/usr/bin/app:rw build-img:$(VERSION) mvn clean package

clean-app:
	docker rmi build-img:$(VERSION)

build-image:
	docker build -t $(IMAGE):$(VERSION) .

docker-login:
	docker login -u $(QUAY_USERNAME) -p $(QUAY_PASSWORD) quay.io

docker-logout:
	docker logout

push-image:
	docker push $(IMAGE):$(VERSION)
	docker rmi $(IMAGE):$(VERSION)

helm-package:
	helm init
	helm package --version $(VERSION) $(APP_NAME)

deploy:
	helm upgrade --install --reuse-values $(APP_NAME) --set image.tag=$(VERSION) $(APP_NAME)-$(VERSION).tgz

clean:
	rm -rf *.tgz